# COPAINSULTES

Copainsultes est un bot Node.js pour messageries instantanées (pour l'instant disponible pour Discord et Twitch) développé sur suggestion de [Copain du Web](https://twitter.com/Copainduweb). Il suffit de poster le message `!insulte` dans un salon, et le bot retournera une insulte générée aléatoirement à partir de bases de mots prises dans des fichiers texte.

L'insulte complète est composée de deux à quatre bases de mots. Ces bases sont stockées par défaut dans le dossier `copainsultes_words`, dans quatre fichiers numérotés, le numéro indiquant la position de la base de mot dans l'insulte.

Pour ajouter (ou enlever) une base de mot, il suffit d'ajouter cette base sur une nouvelle ligne dans le fichier souhaité (ou de supprimer la ligne de la base à retirer), puis d'enregistrer le fichier. Il n'est pas nécessaire de redémarrer le bot ensuite pour qu'il prenne en compte cette nouvelle base.

Le bot Discord permet également une gestion des bases de mots par les membres du serveur (en fonction de leur rôle). Une banlist est proposée, qui refusera automatiquement certains mots à l'ajout (et il est possible d'ajouter ou d'enlever des mots depuis le serveur, ou manuellement en modifiant le fichier texte de la banlist).

## 1. Utilisation avec installation

Il est nécessaire de télécharger et d'installer Node.js sur votre ordinateur/serveur. [Cette page du tutoriel Node.js d'OpenClassrooms](https://openclassrooms.com/fr/courses/1056721-des-applications-ultra-rapides-avec-node-js/1056956-installer-node-js) explique la procédure d'installation sur Windows, Mac OS X et Linux, et les bases du fonctionnement du terminal.

Téléchargez ensuite [l'ensemble de Copainsultes](https://gitlab.com/Desmu/Copainsultes/-/archive/master/Copainsultes-master.zip), décompressez le .ZIP, et rangez le dossier obtenu sur votre ordinateur (il sera nécessaire de redémarrer les bots si le dossier ou ses fichiers sont déplacés, choisissez un emplacement sur votre ordinateur qui ne sera pas amené à changer si possible).

Depuis le terminal, placez vous ensuite dans le dossier Copainsultes-master (il est possible de le renommer), en tapant `cd CHEMIN_DU_DOSSIER` (exemple sur Windows : `cd \Users\Moi\Desktop\mon_dossier_sur_le_bureau\Copainsultes-master`) puis en appuyant sur Entrée. Laissez ensuite la fenêtre du terminal ouverte.

### 1.1. Bot Discord

#### 1.1.1. Création du profil du bot sur Discord.

Sur votre navigateur favori, rendez-vous sur [la page de création de bot Discord](https://discord.com/developers/applications), cliquez sur "Nouvelle application". Nommez le bot "CopainsultesBOT" (ou tout autre nom que vous souhaitez lui donner, ce sera celui qui apparaîtra sur vos serveurs). Ajoutez une description et un avatar si vous le souhaitez, puis cliquez sur "Créer".

Cliquez ensuite sur "Bot" dans le menu, et cliquez sur "Créez un utilisateur Bot". Confirmez, puis cliquez sur "Cliquer pour révéler" au niveau du champ "Jeton". Copiez la suite de chiffres et de lettres qui apparaît alors.

#### 1.1.2. Lancement du bot.

Ouvrez le fichier `copainsultes_discord.js` dans un éditeur de texte comme Bloc-Notes (évitez les logiciels de bureautique qui pourraient provoquer des problèmes à l'enregistrement), et collez la suite de chiffres et de lettres à la ligne 7, entre les apostrophes à la place de COLLEZ_LE_JETON_ICI. Enregistrez et fermez le fichier.

Depuis le terminal laissé ouvert depuis tout à l'heure, tapez `npm install discord.js` puis Entrée (le bot utilise la librairie [Discord.js](https://discord.js.org/) pour fonctionner). À la fin de l'installation, tapez enfin `node copainsultes_discord.js` et laissez la fenêtre du terminal ouverte, pour que le bot se connecte à Discord.
En fonction de votre version de Node.js, il sera peut-être nécessaire de taper plutôt `node --experimental-modules copainsultes_discord.js`.

#### 1.1.3. Utiliser les fonctions de personnalisation.

Ouvrez le fichier `copainsultes_discord.js` dans un éditeur de texte comme Bloc-Notes (évitez les logiciels de bureautique qui pourraient provoquer des problèmes à l'enregistrement), et renseignez la liste des identifiants de rôles de votre serveur qui auront accès aux fonctions de personnalisation de la liste des bases de mots, entre les crochets en ligne 5, à la place de COLLEZ_UN_IDENTIFIANT_DE_ROLE_ADMIN_ICI.
Pour trouver un identifiant de rôle, rendez-vous dans les paramètres utilisateur de Discord, puis dans le menu "Apparence", cherchez la section "Avancés" et cliquez sur "Mode développeur". Toujours sur Discord, faites ensuite un clic droit sur un nom de rôle, et sélectionnez "Copier l'identifiant" pour récupérer l'identifiant dans votre presse-papiers.
Chaque identifiant de rôle doit être renseigné entre apostrophes et suivi d'une virgule (sauf pour le dernier nom renseigné). Enregistrez et fermez le fichier, puis relancez le bot pour que les changements soient pris en compte.

Tout membre du serveur doté d'au moins un des rôles à l'identifiant renseigné dans le fichier pourra dès lors personnaliser les bases de mots du bot depuis le serveur en commençant ses posts par les expressions suivantes :
* `!insulte add CHIFFRE BASE_DE_MOTS` : Ajoute la base de mots BASE_DE_MOTS à la position CHIFFRE (avec CHIFFRE situé entre 1 et 4). Si la base de mot ne comprend pas de mot banni, la base est ajoutée et le bot reposte ensuite la base de mots ajoutée pour confirmer.
* `!insulte list CHIFFRE` : Le bot poste la liste des bases de mots du fichier de la position CHIFFRE.
* `!insulte remove CHIFFRE BASE_DE_MOTS` : Retire la base de mots BASE_DE_MOTS à la position CHIFFRE. Le bot reposte ensuite la liste des bases de mots du fichier de la position CHIFFRE pour confirmer.

L'accès à la personnalisation de la liste des mots bannis fonctionne de manière similaire, pour les membres aux rôles renseignés dans la ligne 6 du fichier du bot :
* `!insulte banadd MOT` : Ajoute le mot MOT à la liste des mots bannis par le bot. Le bot reposte ensuite la liste des mots bannis pour confirmer.
* `!insulte banlist` : Le bot poste la liste des mots bannis.
* `!insulte banremove MOT` : Retire le mot MOT de la liste des mots bannis. Le bot reposte ensuite la liste des mots bannis pour confirmer.
Toutes ces opérations peuvent également être effectuées en modifiant directement les fichiers du dossier copainsultes_words. Chaque mot ou base de mot doit se situer sur une ligne de son fichier TXT.

### 1.2. Bot Twitch

#### 1.2.1. Création du profil du bot sur Twitch.

Commencez par créer un compte Twitch normal pour le bot, en lui donnant le nom de votre choix (vous pouvez réutiliser la même adresse mail pour plusieurs comptes). Gardez ce nom de côté.

Restez connecté à ce compte, rendez-vous sur [la page de génération de mot de passe OAuth Twitch](https://twitchapps.com/tmi/), cliquez sur "Connect" et autorisez l'accès au compte. Copiez la suite de chiffres et de lettres débutant par oauth: qui apparaît alors. Vous pouvez ensuite vous déconnecter du compte du bot.

#### 1.2.2. Lancement du bot.

Ouvrez le fichier `copainsultes_twitch.js` dans un éditeur de texte comme Bloc-Notes (évitez les logiciels de bureautique qui pourraient provoquer des problèmes à l'enregistrement), et collez la suite de chiffres et de lettres (avec la mention oauth:) à la ligne 12, entre les apostrophes à la place de COLLEZ_LE_JETON_ICI.

Placez ensuite le nom donné à votre bot, entre les apostrophes à la ligne 11, à la place de NOM_DU_BOT. Enfin, ajoutez les noms des chaînes auxquelles vous souhaitez que le bot se connecte à la ligne 14 (à la place de CHAINE_1, CHAINE_2, ...). Chaque nom de chaîne doit être renseigné entre apostrophes et suivi d'une virgule (sauf pour le dernier nom renseigné). Enregistrez et fermez le fichier.

Depuis le terminal laissé ouvert depuis tout à l'heure, tapez `npm install tmi.js` puis Entrée (le bot utilise la librairie [TMI.js](https://tmijs.com/) pour fonctionner). À la fin de l'installation, tapez enfin `node copainsultes_twitch.js` et laissez la fenêtre du terminal ouverte, pour que le bot se connecte à Twitch.
En fonction de votre version de Node.js, il sera peut-être nécessaire de taper plutôt `node --experimental-modules copainsultes_twitch.js`.

# 2. Redémarrer un bot.

Par défaut, il est nécessaire de redémarrer un bot à chaque redémarrage de l'ordinateur ou du serveur. Il suffit de se rendre dans le dossier Copainsultes-master depuis le terminal, de lancer la commande `node NOM_DU_FICHIER_DU_BOT.js` (ou `node --experimental-modules NOM_DU_FICHIER_DU_BOT.js`) et de laisser la fenêtre du terminal ouverte. Pour le couper, il faut fermer la fenêtre du terminal (ou utiliser le raccourci Ctrl-C). Pour lancer plusieurs bots, il est nécessaire de laisser plusieurs fenêtres de terminal ouvertes ou d'utiliser un gestionnaire de tâches supplémentaire comme [pm2](https://pm2.keymetrics.io/).
