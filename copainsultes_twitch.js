import fs from "fs";
import tmi from "tmi.js";

const folder = "copainsultes_words/";

const tmiConfig = {
    "connection": {
        "reconnect": true
    },
    "identity": {
        "username": "NOM_DU_BOT",
        "password": "COLLEZ_LE_JETON_ICI"
    },
    "channels": ["CHAINE_1", "CHAINE_2"]
};

const client = new tmi.client(tmiConfig);

client.connect().catch(console.error);

client.on("chat", (channel, user, message, isSelf) => {
    if (message === "!insulte") {
        let data = "";
        let mots = [];
        let insulte = "";
        let partinsulte = "";
        const nombremots = Math.floor(Math.random() * 3) + 2;
        const tabvoyelles = ["a", "à", "â", "ä", "e", "é", "è", "ê", "ë", "i", "î", "ï", "o", "ô", "ö", "u", "û", "ü", "y"];
        for (let i = 1; i <= nombremots; i += 1) {
            data = fs.readFileSync(`${folder}copainsultes_${i}.txt`, "utf8");
            mots = data.split("\n");
            partinsulte = "";
            while (partinsulte === "") {
                partinsulte = mots[Math.floor(Math.random() * mots.length)];
            }
            if (i === 2) {
                for (let j = 0; j < tabvoyelles.length; j += 1) {
                    if ((partinsulte[0] === tabvoyelles[j]) && (insulte[insulte.length - 2] === "e")) {
                        insulte = insulte.substr(0, insulte.length - 2);
                        break;
                    }
                }
            }
            insulte += `${partinsulte} `;
        }
        insulte += "!";
        client.say(channel, insulte);
    }
});

process.on("error", (err) => {
    console.log(err);
});
