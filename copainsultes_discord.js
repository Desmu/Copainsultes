import fs from "fs";
import Discord from "discord.js";

const folder = "copainsultes_words/";
const roles = ["COLLEZ_UN_IDENTIFIANT_DE_ROLE_ADMIN_ICI"];
const rolesBanlist = ["COLLEZ_UN_IDENTIFIANT_DE_ROLE_ADMIN_ICI"];
const token = "COLLEZ_LE_JETON_ICI";

const client = new Discord.Client({"intents": [Discord.Intents.FLAGS.GUILDS, Discord.Intents.FLAGS.GUILD_MESSAGES]});
const tabvoyelles = ["a", "à", "â", "ä", "e", "é", "è", "ê", "ë", "i", "î", "ï", "o", "ô", "ö", "u", "û", "ü", "y"];

function add (msg, mot, file) {
    fs.appendFile(file, `\n${mot}`, (err) => {
        if (err) {
            throw err;
        } else if (file === `${folder}copainsultes_banlist.txt`) {
            msg.channel.send({"content": "**MOT BANNI AJOUTÉ ! VOILÀ LA LISTE DES MOTS BANNIS : **", "files": [file]}).catch(console.error);
        } else {
            msg.channel.send({"content": `**${mot.toUpperCase()}**`}).catch(console.error);
        }
    });
}

function check (msg, mot, chiffre) {
    let banni = false;
    let tabmots = [];
    fs.readFile(`${folder}copainsultes_banlist.txt`, "utf8", (err, data) => {
        if (err) {
            throw err;
        }
        tabmots = data.split("\n");
        for (let i = 0; i < tabmots.length; i += 1) {
            if (tabmots[i] === "") {
                tabmots.splice(i, 1);
                i -= 1;
            } else if (mot.indexOf(tabmots[i]) !== -1) {
                banni = true;
            }
        }
        if (!banni) {
            add(msg, mot, `${folder}copainsultes_${chiffre}.txt`);
        }
    });
}

function remove (msg, mot, file) {
    let mots = [];
    let tabmots = [];
    fs.readFile(file, "utf8", (err, data) => {
        if (err) {
            throw err;
        }
        tabmots = data.split("\n");
        for (let i = 0; i < tabmots.length; i += 1) {
            if ((tabmots[i] === "") || (tabmots[i] === mot)) {
                tabmots.splice(i, 1);
                i -= 1;
            }
        }
        mots = tabmots.join("\n");
        fs.writeFile(file, mots, (er) => {
            if (er) {
                throw er;
            } else {
                msg.channel.send({"content": "**MOT SUPPRIMÉ ! VOILÀ LA LISTE DES MOTS MISE À JOUR : **", "files": [file]}).catch(console.error);
            }
        });
    });
}

client.login(token).catch(console.error);

client.on("messageCreate", (msg) => {
    if (msg.content.indexOf("!insulte") === 0) {
        let admin = false;
        let adminBanlist = false;
        let chiffre = 0;
        let data = "";
        let insulte = "";
        let mot = "";
        let mots = [];
        let nombremots = 0;
        let partinsulte = "";
        if (msg.content === "!insulte") {
            nombremots = Math.floor(Math.random() * 3) + 2;
            for (let i = 1; i <= nombremots; i += 1) {
                data = fs.readFileSync(`${folder}copainsultes_${i}.txt`, "utf8");
                mots = data.split("\n");
                partinsulte = "";
                while (partinsulte === "") {
                    partinsulte = mots[Math.floor(Math.random() * mots.length)].toUpperCase();
                }
                if (i === 2) {
                    for (let j = 0; j < tabvoyelles.length; j += 1) {
                        if ((partinsulte[0] === tabvoyelles[j]) && (insulte[insulte.length - 2] === "e")) {
                            insulte = insulte.substr(0, insulte.length - 2);
                            break;
                        }
                    }
                }
                insulte += `${partinsulte} `;
            }
            insulte += "!";
            msg.channel.send({"content": `**${insulte}**`}).catch(console.error);
        } else if (msg.member._roles) {
            for (let i = 0; i < roles.length; i += 1) {
                if (msg.member._roles.find((role) => role === roles[i])) {
                    admin = true;
                    break;
                }
            }
            if (admin) {
                if (msg.content.indexOf("!insulte add ") === 0) {
                    chiffre = msg.content[13];
                    if ((parseInt(chiffre, 10) > 0) && (parseInt(chiffre, 10) < 5)) {
                        mot = msg.content.replace(`!insulte add ${chiffre} `, "");
                        check(msg, mot, chiffre);
                    }
                } else if (msg.content.indexOf("!insulte list ") === 0) {
                    chiffre = msg.content.replace("!insulte list ", "");
                    msg.channel.send({"content": "**VOILÀ LA LISTE DES MOTS : **", "files": [`${folder}copainsultes_${chiffre}.txt`]}).catch(console.error);
                } else if (msg.content.indexOf("!insulte remove ") === 0) {
                    chiffre = msg.content[16];
                    if ((parseInt(chiffre, 10) > 0) && (parseInt(chiffre, 10) < 5)) {
                        mot = msg.content.replace(`!insulte remove ${chiffre} `, "");
                        remove(msg, mot, `${folder}copainsultes_${chiffre}.txt`);
                    }
                }
            }
            for (let i = 0; i < roles.length; i += 1) {
                if (msg.member._roles.find((role) => role === rolesBanlist[i])) {
                    adminBanlist = true;
                    break;
                }
            }
            if (adminBanlist) {
                if (msg.content.indexOf("!insulte banadd ") === 0) {
                    mot = msg.content.replace("!insulte banadd ", "");
                    add(msg, mot, `${folder}copainsultes_banlist.txt`);
                } else if (msg.content === "!insulte banlist") {
                    msg.channel.send({"content": "**VOILÀ LA LISTE DES MOTS BANNIS : **", "files": [`${folder}copainsultes_banlist.txt`]}).catch(console.error);
                } else if (msg.content.indexOf("!insulte banremove ") === 0) {
                    mot = msg.content.replace("!insulte banremove ", "");
                    remove(msg, mot, `${folder}copainsultes_banlist.txt`);
                }
            }
        }
    }
});

process.on("error", (err) => {
    console.log(err);
});
